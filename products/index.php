<?php
    require_once __DIR__.'/../model/class/product.php';
    require_once __DIR__.'/../model/dao/productsDAO.php';

    //Headers
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

    $requestMethod = $_SERVER['REQUEST_METHOD'];
    
    switch($requestMethod) {
        case 'POST' : {

            try {
                
                if(empty($_POST['codProduct'])) {
                        if(!empty($_POST['nameProduct'] && !empty($_FILES))
                    ) {
                        $arrayProduct = array(
                            'nameProduct' => $_POST['nameProduct'],
                            'descriptionProduct' => $_POST['descriptionProduct'],
                            'pathImgProduct' => time().'.jpg'
                        );
                        
                        $target_dir = '../img/';

                        $target_file = $target_dir.basename(time().'jpg');
                        if(move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
                            $newProduct = new Product($arrayProduct);
                            $productsDAO = new ProductsDAO();
                            $productsDAO->registerProduct($newProduct);

                            http_response_code(200);
                            echo json_encode(array(
                                'message' => 'Success record new product '.$newProduct->getNameProduct()
                            ));
                        } else {
                            http_response_code(400);
                            echo json_encode(array(
                                'message' => 'Failed updaload img'
                            ));
                        }
            
                    } else {
                        http_response_code(400);
                        echo json_encode(array(
                            'message' => 'Icorrect parameters'
                        ));
                    }
                } else {
                    if(empty($_FILES)) {
                        if(!empty($_POST)) {
                            $arrayUpdateProduct = array(
                                'codProduct' => $_POST['codProduct'],
                                'nameProduct' => $_POST['nameProduct'],
                                'descriptionProduct' => $_POST['descriptionProduct'],
                                'pathImgProduct' => $_POST['pathImgProduct']
                            );
    
                            $updateProduct = new Product($arrayUpdateProduct);
                            $productsDAO = new ProductsDAO();
                            $productsDAO->updateProduct($updateProduct);
                            http_response_code(200);
                            echo json_encode(array('message' => 'Product with cod: '.$updateProduct->getCodProduct().' updated with successfuly !!!' ));
                        } else {
                            http_response_code(400);
                            echo json_encode(array('message' => 'Missing parameters' ));
                        }
                    } else {
                        if(!empty($_POST)) {
                            $arrayUpdateProduct = array(
                                'codProduct' => $_POST['codProduct'],
                                'nameProduct' => $_POST['nameProduct'],
                                'descriptionProduct' => $_POST['descriptionProduct'],
                                'pathImgProduct' => $_POST['pathImgProduct']
                            );
                            // print_r($arrayUpdateProduct);
                            $target_dir = '../img/';
    
                        $target_file = $target_dir.basename($arrayUpdateProduct['pathImgProduct']);
                        if(move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
                            $updateProduct = new Product($arrayUpdateProduct);
                            // print_r($updateProduct);
                            $productsDAO = new ProductsDAO();
                            $productsDAO->updateProduct($updateProduct);
                            http_response_code(200);
                            echo json_encode(array('message' => 'Product with cod: '.$updateProduct->getCodProduct().' updated with successfuly !!!' ));
                        } else {
                            http_response_code(400);
                            echo json_encode(array(
                                'message' => 'Failed updaload img in update'
                            ));
                        }
                        }
                    }
                }
                
            } catch (Exception $e) {
                http_response_code(400);
                echo json_encode(array(
                    'message' => $e->getMessage()
                ));
            }
        } break;

        case 'GET' : {

            try {
                $pathRequestGet = explode('/', $_SERVER['REQUEST_URI']);

                if (count($pathRequestGet) == 2) {
                    $productsDAO = new ProductsDAO();
                    $result = $productsDAO->listAllProducts();
                    http_response_code(200);
                    echo json_encode($result);
                    return;

                } else if(count($pathRequestGet) == 3) {
                    $productsDAO = new ProductsDAO();
                    $result = $productsDAO->getProduct(end($pathRequestGet));
                    http_response_code(200);
                    echo json_encode($result);
                    return;

                } else {
                    http_response_code(400);
                    echo json_encode(array('message' => 'Bad Request :( !!!!!'));
                    return;
                }
            } catch (Exception $e) {
                http_response_code(400);
                echo json_encode(array('message' => 'Bad Request :( !!!!!'));
            }
        } break;

        case 'PUT' : {
            try {
               
            } catch (Exception $e) {
                echo json_encode(array('message' => 'Bad Request :( !!!!!!!'));    
            }
            
        } break;

        case 'DELETE' : {
            http_response_code(200);
            echo json_encode(array('message' => 'DELETE'));
        } break;

        default : {
            http_response_code(500);
            echo json_encode(array('message' => 'Internal Server Error. Bad Request :( !!!!'));
        }
    }
?>
