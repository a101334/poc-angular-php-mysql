<?php
    class Product {
        private $codProduct;
        private $nameProduct;
        private $descriptionProduct;
        private $pathImgProduct;

        function __construct($arrayProduct) {

            $this->setCodProduct($arrayProduct['codProduct']);
            $this->setNameProduct($arrayProduct['nameProduct']);
            $this->setDescriptionProduct($arrayProduct['descriptionProduct']);
            $this->setPathImgProduct($arrayProduct['pathImgProduct']);
        }

        //Setters
        public function setCodProduct($codProduct) {
            $this->codProduct = $codProduct;
        }

        public function setNameProduct($nameProduct) {
            $this->nameProduct = $nameProduct;
        }

        public function setDescriptionProduct($descriptionProduct) {
            $this->descriptionProduct = $descriptionProduct;
        }

        public function setPathImgProduct($pathImgProduct) {
            $this->pathImgProduct = $pathImgProduct;
        }

        //Getters
        public function getCodProduct() {
            return $this->codProduct;
        }

        public function getNameProduct() {
            return $this->nameProduct;
        }

        public function getDescriptionProduct() {
            return $this->descriptionProduct;
        }

        public function getPathImgProduct() {
            return $this->pathImgProduct;
        }
    }

?>