<?php
    require_once __DIR__.'/genericDAO.php';
    require_once __DIR__.'/../class/product.php';

    class ProductsDAO extends GenericDAO {
        
        function registerProduct(Product $newProduct) {

            try {
                $this->getConnection();

                $sqlInsert = "INSERT INTO poc_products.products (nameProduct, descriptionProduct, pathImgProduct) VALUES (:nameProduct, :descriptionProduct, :pathImgProduct)";

                $stm = $this->conn->prepare($sqlInsert);

                $stm->bindParam(':nameProduct', $newProduct->getNameProduct());
                $stm->bindParam(':descriptionProduct', $newProduct->getDescriptionProduct());
                $stm->bindParam(':pathImgProduct', $newProduct->getPathImgProduct());
                $stm->execute();

                $this->closeConnection();

            } catch (PDOException $e) {
                echo 'Error: '.$e->getMessage();
            }
            
        }

        function listAllProducts() {
            
            try {

                $this->getConnection();

                $sqlQuery = "SELECT * FROM poc_products.products";
                $stm = $this->conn->prepare($sqlQuery);
                $stm->execute();
                $result = $stm->fetchAll(PDO::FETCH_ASSOC);

                $this->closeConnection();

                return $result;
            } catch (PDOException $e) {
                echo "Error: ".$e->getMessage();
            } 
        }

        function getProduct($cod) {
            
            try {

                $this->getConnection();

                $sqlQuery = "SELECT * FROM poc_products.products WHERE codProduct = :cod";
                $stm = $this->conn->prepare($sqlQuery);
                $stm->bindParam(':cod', $cod);
                $stm->execute();
                $result = $stm->fetchAll(PDO::FETCH_ASSOC);
                $this->closeConnection();

                return $result;
            } catch (Exception $e) {
                echo "Error: ".$e->getMessage();
            } 
        }

        function updateProduct(Product $updateProduct) {

            try {
                $this->getConnection();
                
                $sqlInsert = "UPDATE poc_products.products SET nameProduct = :nameProduct, descriptionProduct = :descriptionProduct, pathImgProduct = :pathImgProduct WHERE codProduct = :codProduct";

                $stm = $this->conn->prepare($sqlInsert);

                $stm->bindParam(':nameProduct', $updateProduct->getNameProduct());
                $stm->bindParam(':descriptionProduct', $updateProduct->getDescriptionProduct());
                $stm->bindParam(':pathImgProduct', $updateProduct->getPathImgProduct());
                $stm->bindParam(':codProduct', $updateProduct->getCodProduct());
                $stm->execute();

                $this->closeConnection();

            } catch (PDOException $e) {
                echo 'Error: '.$e->getMessage();
            }
            
        }
        
    }

?>