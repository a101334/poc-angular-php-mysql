<?php
    require_once __DIR__.'/../../config.php';

    class genericDAO {
        private $host;
        private $userName;
        private $password;
        protected $conn;

        function __construct() {
            $this->setHost(HOST_DB);
            $this->setUserName(USER_DB);
            $this->setPassword(PASSWORD_DB);
            $this->conn = null;
        }

        public function getConnection() {

            try {            
                $this->conn = new PDO('mysql:host='.$this->getHost(), $this->getUserName(), $this->getPassword());
                // echo "Connection successfuly !!!!";
            } catch (PDOException $e) {
                echo 'Error: '.$e->getMessage();
            }
        }

        public function closeConnection() {
            if(!is_null($this->conn)) {
                $this->conn = null;
            }
        }

        //Setters
        public function setHost($host) {
            $this->host = $host;
        }

        public function setUserName($userName) {
            $this->userName = $userName;
        }

        public function setPassword($password) {
            $this->password = $password;
        }

        //Getters
        public function getHost() {
            return $this->host;
        }

        public function getUserName() {
            return $this->userName;
        }

        public function getPassword() {
            return $this->password;
        }
    }

?>