DROP DATABASE IF EXISTS poc_products;
CREATE DATABASE poc_products;

DROP TABLE IF EXISTS poc_products.products;
CREATE TABLE poc_products.products (
    codProduct INT NOT NULL AUTO_INCREMENT,
    nameProduct VARCHAR(30) NOT NULL,
    descriptionProduct VARCHAR(255),
    pathImgProduct VARCHAR(255) NOT NULL,
    PRIMARY KEY (codProduct)
);

INSERT INTO poc_products.products (nameProduct, pathImgProduct)
VALUES 
("Esfirra", "img/esfirra.jpg"),
("Pão", "img/pao.jpg"),
("Bolo", "img/bolo.jpg");

SELECT * FROM poc_products.products;